import sqlite3
import os

import numpy


class ConnectWithSqlite:
    def __init__(self):
        try:
            BASE_DIR = os.path.dirname(os.path.abspath(__file__))
            db_path = os.path.join(BASE_DIR, "attendance.db")
            self.connection = sqlite3.connect(db_path)

        except sqlite3.Error as error:
            print("Error while connecting to sqlite", error)

    def insertData(self, name, encode):
        cursor = self.connection.cursor()

        sql = 'insert into employers (name, encode) values (?, ?)'
        # for i in encodeArray:
        cursor.execute(sql, (name, encode,))

        self.connection.commit()

        cursor.close()

    def get_all(self):
        cursor = self.connection.cursor()

        items = cursor.execute('select * from employers')
        self.connection.commit()

        row = items.fetchall()

        names = [i[1] for i in row]
        arr = []
        arr2 = []
        numpy_array = []
        s = ''

        for i in row:
            for j in i[2]:
                if not (j == '[' or j == ']' or not j):
                    s += j

            arr.append(s.split())
            s = ''

        for i in arr:
            for j in i:
                arr2.append(float(j))

            numpy_array.append(arr2)
            arr2 = []

        return names, numpy.array(numpy_array)

    def close(self):
        self.connection.close()
