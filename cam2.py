import cv2 as cv
import numpy as np
import face_recognition
import os
import asyncio
import datetime
from py_stlite_example.connect_sqlite import ConnectWithSqlite
from playsound import playsound

sqliteConnectionExample = ConnectWithSqlite()

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path = os.path.join(BASE_DIR, "photos")
# path = 'photos'
images = []
classNames = []
myList = os.listdir(path)
print(myList)

dtToday = datetime.datetime.today()
file_enter = os.path.join(BASE_DIR, f'excels/Attendance {dtToday.day}.{dtToday.month}.{dtToday.year} Enter.csv')
file_exit = os.path.join(BASE_DIR, f'excels/Attendance {dtToday.day}.{dtToday.month}.{dtToday.year} Exit.csv')

f_enter = open(file_enter, 'a+')
f_exit = open(file_exit, 'a+')
# f.writelines('Full Name, Time, Action')

for cl in myList:
    curImg = cv.imread(f'{path}/{cl}')
    # images.append(curImg)
    name = os.path.splitext(cl)[0]
    img = cv.cvtColor(curImg, cv.COLOR_BGR2RGB)
    encode = face_recognition.face_encodings(img)[0]
    sqliteConnectionExample.insertData(name, np.array_str(encode))
    os.remove(f'{path}/{cl}')

last_name = ''


def markAttendanceEnter(name):
    with open(file_enter, 'r+') as f:
        myDataList = f.readlines()
        nameList = []

        for line in myDataList:
            entry = line.split(',')
            nameList.append(entry[0])

        now = datetime.datetime.now()
        dtString = now.strftime('%H:%M:%S')
        if name not in nameList:
            f.writelines(f'\n{name}, {dtString}')
            playsound('sounds/error.mp3')


peopleNames, encodeListKnown = sqliteConnectionExample.get_all()
print('Encoding Completed')

cap2 = cv.VideoCapture(0)
# cap2 = cv.VideoCapture('rtsp://admin:z12345678@192.168.1.131/1')

while True:
    ret, frame = cap2.read()

    if ret:
        imgS = cv.resize(frame, (0, 0), None, 0.25, 0.25)
        imgS = cv.cvtColor(imgS, cv.COLOR_BGR2RGB)

        facesCurrentFrame = face_recognition.face_locations(imgS)
        encodeCurFrame = face_recognition.face_encodings(imgS, facesCurrentFrame)

        for encodeFace, faceLoc in zip(encodeCurFrame, facesCurrentFrame):
            matches = face_recognition.compare_faces(encodeListKnown, encodeFace)
            faceDis = face_recognition.face_distance(encodeListKnown, encodeFace)
            # print(faceDis)
            matchIndex = np.argmin(faceDis)

            if matches[matchIndex] and faceDis[matchIndex] < 0.5:
                name = peopleNames[matchIndex]
                y1, x2, y2, x1 = faceLoc
                y1, x2, y2, x1 = y1 * 4, x2 * 4, y2 * 4, x1 * 4
                cv.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
                cv.rectangle(frame, (x1, y2 - 35), (x2, y2), (0, 255, 0), cv.FILLED)
                cv.putText(frame, name, (x1 + 6, y2 - 6), cv.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 2)

                markAttendanceEnter(name=name)
            else:
                y1, x2, y2, x1 = faceLoc
                y1, x2, y2, x1 = y1 * 4, x2 * 4, y2 * 4, x1 * 4
                cv.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 2)
                playsound('sounds/sound.mp3')

        cv.imshow('Parvoz Humo Ravnaq Trans', frame)

    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap2.release()
cv.destroyAllWindows()
sqliteConnectionExample.close()
